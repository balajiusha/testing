package com.usha.online.testing;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
	WebDriver driver= null;
	@Override
	protected void setUp() throws Exception{
		//setting driver properties
		System.out.println("Setting it up!");
		System.setProperty("webdriver.chrome.driver", "C:\\Jenkins\\workspace\\testing\\online\\chromedriver.exe");
		driver=new ChromeDriver();
	}
    /**
     *Create the test case
     *Launch Chrome
     *Select Fashion-->Shoes
     *Click on Myer under Featured Stores
     *Enter New moki kids and click search
     *Click on Purple Headphone
     *Verify the user is viewing product description and price 
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    
  
        public void testScenario1() {
		driver.get("https://www.ebay.com.au/");	
		driver.manage().window().maximize();

        driver.findElement(By.xpath("//*[@id=\"s0-container\"]/li[3]/a")).click();
		driver.findElement(By.xpath("//*[@id=\"mainContent\"]/div[3]/ul/div[1]/li/ul/li[4]/a")).click();
		driver.findElement(By.xpath("//*[@id=\"mainContent\"]/div[3]/ul/div[2]/li/ul/li[6]/a/span")).click();
		driver.findElement(By.xpath("//*[@id=\"form-finder\"]/input[1]")).sendKeys("New moki kids");
		driver.findElement(By.xpath("//*[@id=\"form-finder\"]/input[2]")).click();
		JavascriptExecutor js= (JavascriptExecutor)driver;
		js.executeScript("scrollBy(0, 2500);");
		driver.findElement(By.xpath("//*[@id=\"src232330394408\"]/img")).click();
		
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     }
	/*public void testScenario2() {
			driver.get("https://www.ebay.com.au");		
			driver.manage().window().maximize();
			
			driver.findElement(By.xpath("//*[@id=\"gh-ug-flex\"]/a")).click();
			driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys("Usha rani");
			driver.findElement(By.xpath("//*[@id=\"lastname\"]")).sendKeys("Kalanati");
			driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys("usha.kalanati@gmail.com");
			driver.findElement(By.xpath("//*[@id=\"PASSWORD\"]")).sendKeys("viyaan123#");
		    driver.findElement(By.xpath("").sendKeys("viyaan123#");

			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }*/
      
    public void testScenario3() {
		driver.get("https://www.ebay.com.au/");		
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//*[@id=\"gh-as-a\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"_nkw\"]")).sendKeys("Bag");
		driver.findElement(By.xpath("//*[@id=\"adv_search_from\"]/fieldset[3]/input[2]")).sendKeys("$1000");
		driver.findElement(By.xpath("//*[@id=\"adv_search_from\"]/fieldset[3]/input[3]")).sendKeys("$2000");
		JavascriptExecutor js= (JavascriptExecutor)driver;
		js.executeScript("scrollBy(0, 2500);");
		driver.findElement(By.xpath("//*[@id=\"LH_Auction\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"adv_search_from\"]/fieldset[1]/div[3]/button")).click();


		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    }
	
    @Override
    protected void tearDown() throws Exception {
        System.out.println("Running: tearDown");
        if(driver != null) {
            driver.close();
        }
    }
    
}
